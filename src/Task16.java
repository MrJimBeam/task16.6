

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Task16 {
    private static List<String> theFile;
    public static void main(String[] args) {
        Scanner mySc = new Scanner(System.in);
        String word = "";
        int count = 0;
        if(args.length < 1){
            System.out.println("Please enter a file name as an argument");
            return;
        }else{
            File file = new File(args[0]);
            if(file.exists()){
                theFile = readFile(Paths.get(args[0]));
                Iterator<String> itr = theFile.iterator();
                System.out.println("The file name is " + args[0] +" and is " + (double)(file.length()) +" bytes " +" the file has " +  theFile.size() + " lines");
            }else{
                System.out.println("File was not found, please try again");
                return;
            }

        }
        System.out.println("please enter a word to search the file");
        while (mySc.hasNext()){
            word=mySc.next();
            break;
        }

        for(String line : theFile){
            String[] parsedLine = line.split(" ");
            for(int i = 0; i<parsedLine.length; i++){
                if(word.toLowerCase().equals(parsedLine[i].toLowerCase())){
                    count ++;
                }
            }
        }
        System.out.println("The word " + word + " was mentiond " + count + " times");
    }

    public static List<String> readFile(Path path){

        List<String> lines = Collections.emptyList();
        try{
            lines = Files.readAllLines(Paths.get(String.valueOf(path)), StandardCharsets.UTF_8);
        }catch (IOException e){
            e.printStackTrace();
        }
        return lines;
    }



}
